import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { ProductListPage } from '../pages/product-list/product-list-page';
import { CartPage } from '../pages/cart/cart-page';

import { HeaderComponent } from '../components/header/header-component'

import { CartService } from '../services/cart-service';

@NgModule({
  declarations: [
    MyApp,
    ProductListPage,
    CartPage,
    HeaderComponent
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [ IonicApp ],
  entryComponents: [
    MyApp,
    ProductListPage,
    CartPage
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    CartService
  ]
})
export class AppModule {}
