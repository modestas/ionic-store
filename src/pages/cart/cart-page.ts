import { Component } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';

import { Product } from '../../models/product';
import { CartService } from '../../services/cart-service';

@Component({
  selector: 'cart-page',
  templateUrl: 'cart-page.html'
})
export class CartPage {

  private subscription: Subscription;
  
  products: Array<Product> = [];
  priceTotal: number = 0;

  constructor(private cartService: CartService,
              private alertController: AlertController) {
    
  }

  ionViewWillEnter() : void {
    this.subscription = this.cartService.products.subscribe(products => this.updateProducts(products));
  }

  ionViewDidLeave() : void {
    this.subscription.unsubscribe();
  }

  removeProduct(product: Product) : void {
    let confirm = this.alertController.create({
      title: 'Remove product?',
      message: 'Do you really want to remove ' + product.name + ' from the cart?',
      buttons: [
        {
          text: 'No'
        },{
          text: 'Yes',
          handler: () => this.cartService.removeProduct(product.name)
        }
      ]
    });
    confirm.present();
  }

  private updateProducts(products: Array<Product>) : void {
    this.products = products;
    this.priceTotal = this.products.reduce((previous: number, current: Product) => previous + current.price, 0)
  }
}
