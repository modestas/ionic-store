import { Component } from '@angular/core';
import { Vibration } from 'ionic-native';

import { Product } from '../../models/product'
import { CartService } from '../../services/cart-service'

@Component({
  selector: 'product-list-page',
  templateUrl: 'product-list-page.html'
})
export class ProductListPage {

  products: Array<Product>;
  filteredProducts: Array<Product>;

  constructor(private cartService: CartService) {
    this.products = [
      { name: "Apple", price: 2.19 },
      { name: "Book", price: 9.99 },
      { name: "Table", price: 59.49 },
      { name: "Pen", price: 0.29 },
      { name: "Apricot", price: 1.30 }
    ];
    this.filter(null);
  }

  filter(name: string) : void {
    if (name) {
      this.filteredProducts = this.products.filter(
        product => product.name.toLowerCase().indexOf(name.toLowerCase()) !== -1
      );
    } else {
      this.filteredProducts = this.products;
    }
  }

  isInCart(product: Product) : boolean {
    return this.cartService.hasProduct(product.name);
  }

  addToCart(product: Product) : void {
    this.vibrate();
    this.cartService.addProduct(product);
  }

  private vibrate() : void {
    Vibration.vibrate(50);
  }
  
}
