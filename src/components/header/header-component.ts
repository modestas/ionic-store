import { Component } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { CartService } from '../../services/cart-service';

@Component({
  selector: 'header-component',
  templateUrl: 'header-component.html'
})
export class HeaderComponent {

  private subscription: Subscription;

  constructor(private cartService: CartService) {
    
  }

  ionViewWillEnter() : void {
    this.subscription = this.cartService.products.subscribe(products => {});
  }

  ionViewDidLeave() : void {
    this.subscription.unsubscribe();
  }
}
