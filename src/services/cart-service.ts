import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { Product } from '../models/product';

@Injectable()
export class CartService {

    private _products: Array<Product> = [];

    products: BehaviorSubject<Array<Product>> = new BehaviorSubject<Array<Product>>([]);

    constructor() {
        
    }

    hasProduct(name: string) : boolean {
        return this._products.find(product => product.name === name) ? true : false;
    }

    addProduct(product: Product) : void {
        if (this.hasProduct(product.name)) {
            return;
        }
        this._products.push(product);
        this.products.next(this._products);
    }

    removeProduct(name: string) : void {
        this._products = this._products.filter(product => product.name !== name);
        this.products.next(this._products);
    }
}