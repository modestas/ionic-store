# ionic-store #

This application was created to test Ionic Framework. It is a simple store containing two pages: product list and cart.

### How do I get set up? ###

* Clone repository
* Download and install Node (tested with version 7.5.0)
* Run `npm install -g cordova ionic`
* Run `npm install` inside cloned directory
* Run `ionic serve` for the first time, so that www folder would be created (issue https://github.com/driftyco/ionic-cli/issues/935 )
* Run `ionic platform add PLATFORM`, replacing PLATFORM with android, ios, windows
* Run `ionic run PLATFORM` to run project on your platform. 

### Project structure ###

* src/app - applications entry point
* src/components - reusable components for pages and other components
* src/models - data classes
* src/pages - application page components
* src/services - singletons for shared data or functionality

### Screenshots ###

![screenshots/Screenshot_20170217-133205.png](screenshots/Screenshot_20170217-133205.png)
![screenshots/Screenshot_20170217-133216.png](screenshots/Screenshot_20170217-133216.png)